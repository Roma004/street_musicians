# -*- coding: utf-8 -*-
from time import time
from uuid import uuid4

from peewee import CharField, IntegerField, Model, PostgresqlDatabase
from peeweedbevolve import *  # noqa

from configs.settings import db_settings

db = PostgresqlDatabase(
    db_settings.name,
    user=db_settings.user_name,
    password=db_settings.password,
    host=db_settings.host,
    port=db_settings.port,
)


def str_uuid():
    return str(uuid4())


class BaseModel(Model):
    uuid = CharField(max_length=64, default=str_uuid, unique=True, primary_key=True)
    timestamp = IntegerField(default=time)

    def update_values(self, data: dict):
        for key, val in data.items():
            if hasattr(self, key):
                setattr(self, key, val)

    class Meta:
        database = db
