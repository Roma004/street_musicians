from peewee import CharField, TimeField, BooleanField, ForeignKeyField

from db.db import BaseModel
from db.construct.user import User


class Performance(BaseModel):
    name = CharField(max_length=70)
    user = ForeignKeyField(User, backref='performances')
    yandex_coord = CharField(max_length=100)
    start_time = TimeField()
    is_active = BooleanField()
