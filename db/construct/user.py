from __future__ import annotations
from peewee import BooleanField, CharField, IntegerField, TextField

from db.db import BaseModel


class User(BaseModel):
    username = CharField(max_length=70)
    email = CharField(max_length=70, index=True)
    password = CharField(null=True)

    access_token = TextField(null=True)
    refresh_token = TextField(null=True)

    @classmethod
    def get_by_jwt(cls, identity: dict) -> User:
        return cls.get_or_none(cls.uuid == identity["id"])


    # vk_user_id = IntegerField(index=True)

    # token = TextField(default="")