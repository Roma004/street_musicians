from peewee import CharField

from db.db import BaseModel


class Socialnet(BaseModel):
    name = CharField(max_length=70)
