from peewee import CharField

from db.db import BaseModel


class Genre(BaseModel):
    name = CharField(max_length=70)
