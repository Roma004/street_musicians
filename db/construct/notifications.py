from peewee import ForeignKeyField, CharField

from db.db import BaseModel
from db.construct.user import User


class Notification(BaseModel):
    user = ForeignKeyField(User, backref='notifications')

    text = CharField() 
