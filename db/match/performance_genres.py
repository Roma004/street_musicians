from peewee import ForeignKeyField

from db.db import BaseModel
from db.construct.performance import Performance
from db.construct.genre import Genre


class PerformanceGenre(BaseModel):
    performance = ForeignKeyField(Performance, backref='genres')

    genre = ForeignKeyField(Genre, backref='performances')
