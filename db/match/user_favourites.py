from peewee import ForeignKeyField, BooleanField

from db.db import BaseModel
from db.construct.user import User


class UserFavourites(BaseModel):
    user = ForeignKeyField(User, backref='favourites')

    favourite = ForeignKeyField(User, backref='users')

    is_notification_allow = BooleanField()
