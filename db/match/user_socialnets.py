from peewee import ForeignKeyField, CharField

from db.db import BaseModel
from db.construct.user import User
from db.construct.socialnets import Socialnet


class UserSocialnets(BaseModel):
    user = ForeignKeyField(User, backref='socialnets')

    socialnet = ForeignKeyField(Socialnet, backref='users')

    adress = CharField(max_length=150)
