from peewee import ForeignKeyField, IntegerField, TextField
from db.db import BaseModel
from db.construct.user import User


class UserReviews(BaseModel):
    user = ForeignKeyField(User, backref='reviews')
    reviewer = ForeignKeyField(User, backref='users')
    review_text = TextField()
    mark = IntegerField()
