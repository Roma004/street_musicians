from peewee import ForeignKeyField, TextField, IntegerField

from db.db import BaseModel
from db.construct.user import User
from db.construct.performance import Performance


class PerformReviews(BaseModel):
    performance = ForeignKeyField(Performance, backref='reviews')
    reviewer = ForeignKeyField(User, backref='performances')
    review_text = TextField()
    mark = IntegerField()
