from peewee import ForeignKeyField

from db.db import BaseModel
from db.construct.user import User
from db.construct.genre import Genre


class FavouriteGenre(BaseModel):
    user = ForeignKeyField(User, backref='genres')

    genre = ForeignKeyField(Genre, backref='users')
