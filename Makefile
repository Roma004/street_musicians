FORMAT_DIRS=configs main.py db decorators errors utils view app.py manage.py 
LINTER_DIRS=configs main.py db decorators errors utils view app.py manage.py 

.PHONY: install
install:
	python -m pip install -r requirements.txt

.PHONY: pretty
pretty: isort black autoflake

.PHONY: black
black:
	python -m black -t py310 $(FORMAT_DIRS)

.PHONY: isort
isort:
	python -m isort $(FORMAT_DIRS)

.PHONY: autoflake
autoflake:
	python -m autoflake -i -r --ignore-init-module-imports --remove-all-unused-imports --expand-star-imports $(FORMAT_DIRS)

.PHONY: mypy
mypy:
	mypy $(LINTER_DIRS)
