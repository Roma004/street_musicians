import requests
from errors import bad_credentials, user_already_exists
from app import bcrypt

from db import User


def login(email: str, password: str) -> User:

    try:
        user: User = User.get(User.email == email, User.password.is_null(False))
    except User.DoesNotExist:
        raise bad_credentials

    if not bcrypt.check_password_hash(user.password, password):
        raise bad_credentials

    return user


def register(email: str, password: str) -> User:
    try:
        user: User = User.get(User.email == email, User.password.is_null(False))
        raise user_already_exists
    except User.DoesNotExist:
        pass

    user: User = User.create(
        username=email[:email.find("@")].replace(".", " "),
        email=email,
        password=bcrypt.generate_password_hash(password).decode()
    )

    return user
