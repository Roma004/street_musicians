from flask_jwt_extended import create_access_token, create_refresh_token

from db import User


def generate_jwt_tokens(user: User) -> dict:
    identity = {"id": user.uuid, "status": "user"}

    return {
        "access_token": create_access_token(identity, fresh=True),
        "refresh_token": create_refresh_token(identity),
    }
