from typing import Callable

from flask_jwt_extended import get_jwt_identity, jwt_required, get_jwt_header
from flask_jwt_extended.exceptions import JWTExtendedException
from errors import not_authenticated

from db import User


@jwt_required(optional=True)
def get_user(required: bool = False) -> Callable:
    try:
        identity = get_jwt_identity()

        if identity is None:
            user = None

        user: User = User.get_or_none(User.uuid == identity["id"])

    except JWTExtendedException:
        user = None

    if required and user is None:
        raise not_authenticated

    print(get_jwt_header())

    return user
