from typing import Callable

from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_jwt_extended.exceptions import JWTExtendedException

from db import User
from typing import Optional


def identify_user(fn) -> Callable:
    @jwt_required(optional=True)
    def wrapped(data: dict) -> dict:

        new_data = data

        try:
            identity = get_jwt_identity()

            if identity is not None:
                new_data["user"] = User.get_or_none(User.uuid == identity["id"])
            else:
                new_data["user"] = None

        except JWTExtendedException:
            new_data["user"] = None

        return fn(new_data)

    return wrapped


class UserMixin:
    user: Optional[User]