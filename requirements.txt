attrs==21.4.0
autoflake==1.7.7
bcrypt==3.2.0
black==22.10.0
certifi==2022.6.15
cffi==1.15.0
charset-normalizer==2.1.0
click==8.1.2
colorama==0.4.4
cryptography==37.0.4
flake8==4.0.1
Flask==2.1.1
Flask-Bcrypt==1.0.1
Flask-Cors==3.0.10
Flask-JWT-Extended==4.3.1
Flask-Pydantic==0.11.0
gunicorn==20.1.0
idna==3.3
isort==5.10.1
itsdangerous==2.1.2
Jinja2==3.1.1
jsonschema==4.4.0
MarkupSafe==2.1.1
mccabe==0.6.1
mypy-extensions==0.4.3
pathspec==0.10.1
peewee==3.14.10
peewee-db-evolve==3.7.6
platformdirs==2.5.2
psycopg2-binary==2.9.4
pycodestyle==2.8.0
pycparser==2.21
pydantic==1.10.2
pyflakes==2.4.0
PyJWT==2.3.0
PyMySQL==1.0.2
pyrsistent==0.18.1
python-dotenv==0.20.0
requests==2.28.1
six==1.16.0
tomli==2.0.1
typing_extensions==4.4.0
urllib3==1.26.10
Werkzeug==2.1.1
