from errors.err_exception import ErrException

error_500 = ErrException("500 Internal Server Error", 500, 500)
error_404 = ErrException("404 Not Found", 404, 404)
error_405 = ErrException("405 Method Not Allowed", 405, 405)

# HTTP 400 BAD REQUEST
error_data = ErrException("Error JSON data", 1000, 400)
error_type = ErrException("__custom__", 1001, 400)


# HTTP 401 UNAUTHORISED
bad_credentials = ErrException("Bad credantials", 1001, 401)
token_burned = ErrException("Token no more viable", 1002, 401)
not_authenticated = ErrException("User is not authenticated", 1003, 401)


# HTTP 422 UNPROCESSABLE ENTRY (custom errors)
user_does_not_exist = ErrException("User Does not exists", 1100)
user_already_exists = ErrException("Such username is in use", 1101)
performance_started = ErrException("You've already started performance", 1102)
no_performance_started = ErrException("You've got no active performances", 1103)
socialnet_does_not_supported = ErrException("Socialnet does not supported", 1104)

