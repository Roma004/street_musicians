from flask import request
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required

from app import app
from errors import error_data
from utils.auth.jwt_utils import generate_jwt_tokens
from utils.auth.login import login, register

from db import User
import json

@app.route("/refresh", methods=["POST"])
@jwt_required(refresh=True)
def refresh_handler():
    identity = get_jwt_identity()
    user = User.get_by_jwt(identity)

    access_token = create_access_token(identity=identity)


    user.access_token = access_token
    user.save()

    return {"access_token": access_token}



@app.route("/login", methods=["POST"])
def login_handler():
    try:
        data = json.loads(request.data)
    except:
        raise error_data

    user = login(data["email"], data["password"])
    
    res = generate_jwt_tokens(user)

    user.access_token = res["access_token"]
    user.refresh_token = res["refresh_token"]
    user.save()

    return res, 200


@app.route("/register", methods=["POST"])
def register_handler():
    try:
        data = json.loads(request.data)
    except:
        raise error_data

    user = register(data["email"], data["password"])
    
    res = generate_jwt_tokens(user)

    user.access_token = res["access_token"]
    user.refresh_token = res["refresh_token"]
    user.save()

    return res, 200


# TODO unauthorize method
