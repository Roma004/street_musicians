from typing import Optional

from flask_pydantic import validate
from pydantic import BaseModel

from app import app


class RequestModel(BaseModel):
    asd: Optional[str]
    page: int


@app.route("/get_info", methods=["GET"])
@validate()
def get_info(query: RequestModel):

    return query.dict(), 200
