from typing import Optional

from flask_pydantic import validate
from uuid import UUID
from pydantic import BaseModel

from app import app


class RequestModel(BaseModel):
    genres: list[UUID]


@app.route("/set_favourite_genres", methods=["POST"])
@validate()
def set_favourite_genres(body: RequestModel):

    print(body.dict())

    if not True:

        ...

    return body.dict(), 200
