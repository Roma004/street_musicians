from pydantic import AnyHttpUrl, BaseSettings, Extra


class CustomBaseSettings(BaseSettings):
    class Config:
        case_sensitive = False
        env_file = ".env"
        env_file_encoding = "utf-8"
        extra = Extra.ignore


class DatabaseSettings(CustomBaseSettings):
    name: str
    user_name: str
    password: str
    host: str = "localhost"
    port: int = 5432

    class Config:
        env_prefix = "db_"


class ServiceSettings(CustomBaseSettings):
    pass


db_settings = DatabaseSettings()
settings = ServiceSettings()
