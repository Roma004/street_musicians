import datetime
import os

from dotenv import load_dotenv

dot_env_path = os.path.join(os.path.dirname(__file__), "../.env")
if os.path.exists(dot_env_path):
    load_dotenv(dot_env_path)


class BaseConfig:
    __conf_type = os.environ.get("CONFIG_ENV")

    JWT_SECRET_KEY = os.environ.get("JWT_SECRET_KEY")

    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(
        seconds=int(os.environ.get("JWT_ACCESS_TOKEN_EXPIRES_TIMEDELTA"))
    )
    JWT_REFRESH_TOKEN_EXPIRES = datetime.timedelta(
        days=int(os.environ.get("JWT_REFRESH_TOKEN_EXPIRES_TIMEDELTA"))
    )

    JWT_TOKEN_LOCATION = ["headers"]


class DevelopmentConfig(BaseConfig):
    USE_CORS: bool = False

    DEBUG = True

    CORS: dict = {
        r"/private/*": {"origins": "*"},
        "/auth": {"origins": "*"},
        "/refresh": {"origins": "*"},
    }


class ProductionConfig(BaseConfig):
    DEBUG = False

    USE_CORS: bool = True

    CORS: dict = {
        r"/private/*": {"origins": "*"},
        "/auth": {"origins": "*"},
        "/refresh": {"origins": "*"},
    }
