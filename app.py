import os
import traceback

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from configs import flask_config
from errors import error_404, error_405, error_500
from errors.err_exception import ErrException, get_error

import json

# -------------- END -------------------

app: Flask = Flask(__name__)

jwt = JWTManager(app)

config_env: str = os.environ.get("CONFIG_ENV")
if hasattr(flask_config, config_env):
    if config_env == "DevelopmentConfig":
        os.environ["FLASK_ENV"] = "development"

    app.config.from_object(getattr(flask_config, config_env))
else:
    print("Use config class form config.py")
    exit()


if app.config["USE_CORS"]:
    CORS(app, resources=app.config["CORS"])

bcrypt = Bcrypt(app)


@app.errorhandler(ErrException)
def custom_error_handler(error):
    err = get_error(error)
    return err


@app.errorhandler(Exception)
def handler_500(error: Exception) -> tuple[dict, int]:
    print(traceback.format_exc())

    return dict(error_500), 500


@app.errorhandler(404)
def handler_404(error) -> tuple[dict, int]:
    return dict(error_404), 404


@app.errorhandler(405)
def handler_405(error) -> tuple[dict, int]:
    return dict(error_405), 405
